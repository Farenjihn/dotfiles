$env.config.show_banner = false

$env.config.table = {
    mode: "light"
    show_empty: false
}

$env.config.completions.algorithm = "fuzzy"

$env.config.history.file_format = "sqlite"
$env.config.history.isolation = true

$env.config.ls.clickable_links = false
$env.config.rm.always_trash = false

use integrations
use modules *

abbr vim "nvim"

abbr ll "ls"
abbr l "ls -a"
