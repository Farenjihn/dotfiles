export-env {
    load-env {
        XDG_CACHE_HOME: ($env.HOME | path join ".cache")
        XDG_CONFIG_HOME: ($env.HOME | path join ".config")
        XDG_DATA_HOME: ($env.HOME | path join ".local/share")
        XDG_STATE_HOME: ($env.HOME | path join ".local/state")
        XDG_DOCUMENTS_DIR: ($env.HOME | path join "documents")
        XDG_DOWNLOAD_DIR: ($env.HOME | path join "downloads")
        XDG_MUSIC_DIR: ($env.HOME | path join "music")
        XDG_PICTURES_DIR: ($env.HOME | path join "images")
        XDG_VIDEOS_DIR: ($env.HOME | path join "videos")
    }
}

export-env {
    load-env {
        CARGO_HOME: ($env.XDG_DATA_HOME | path join "cargo")
        CARGO_INSTALL_ROOT: ($env.HOME | path join ".local")
        GOPATH: ($env.XDG_DATA_HOME | path join "go")
        RUSTUP_HOME: ($env.XDG_DATA_HOME | path join "rustup")
    }
}

# path
use std/util "path add"
$env.PATH = ($env.PATH | split row (char esep))

path add ($env.HOME | path join ".krew/bin")
path add ($env.HOME | path join ".local/bin")

$env.PATH = ($env.PATH | uniq)

# utils
$env.BROWSER = "firefox"
$env.EDITOR = "nvim"

$env.REGISTRY_AUTH_FILE = ($env.HOME | path join ".config/containers/auth.json")
$env.SSH_AUTH_SOCK = ($env.XDG_RUNTIME_DIR | path join "ssh-agent.socket")

# wayland
$env.XDG_CURRENT_DESKTOP = "sway"

$env.CLUTTER_BACKEND = "wayland"
$env.QT_QPA_PLATFORM = "wayland"
$env.SDL_VIDEODRIVER = "wayland"
$env.MOZ_ENABLE_WAYLAND = "1"

# colors
$env.LS_COLORS = (dircolors -b | split row "'" | get 1)
