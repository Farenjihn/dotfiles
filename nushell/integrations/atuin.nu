const ATUIN_KEYBINDING_TOKEN = "# 7a1d4b53-7706-4396-a633-9f18ef4a162a"

const ATUIN_SEARCH_CMD = [
    $ATUIN_KEYBINDING_TOKEN,
    "with-env { ATUIN_LOG: error, ATUIN_QUERY: (commandline) } {",
    "    commandline edit (run-external 'atuin' 'search' '--interactive' e>| str trim)",
    "}",
] | str join "\n"

export-env {
    let atuin_pre_execution = {||
        if ($nu | get -i history-enabled) == false {
            return
        }

        let cmd = (commandline)
        if ($cmd | is-empty) {
            return
        }

        if not ($cmd | str starts-with $ATUIN_KEYBINDING_TOKEN) {
            $env.ATUIN_HISTORY_ID = (atuin history start -- $cmd)
        }
    }

    let atuin_pre_prompt = {||
        let last_exit = $env.LAST_EXIT_CODE
        if 'ATUIN_HISTORY_ID' not-in $env {
            return
        }

        with-env { ATUIN_LOG: error } {
            do { atuin history end $'--exit=($last_exit)' -- $env.ATUIN_HISTORY_ID } | complete

        }

        hide-env ATUIN_HISTORY_ID
    }

    $env.ATUIN_SESSION = (atuin uuid)
    hide-env -i ATUIN_HISTORY_ID

    $env.config = ($env | default {} config).config
    $env.config = ($env.config | default {} hooks)
    $env.config = (
        $env.config | upsert hooks (
            $env.config.hooks
            | upsert pre_execution (
                $env.config.hooks.pre_execution? | default [] | append $atuin_pre_execution
            )
            | upsert pre_prompt (
                $env.config.hooks.pre_prompt? | default [] | append $atuin_pre_prompt
            )
        )
    )

    $env.config = ($env.config | default [] keybindings)
    $env.config = (
        $env.config | upsert keybindings (
            $env.config.keybindings | append {
                name: atuin
                modifier: control
                keycode: char_r
                mode: [emacs, vi_normal, vi_insert]
                event: {
                    send: executehostcommand
                    cmd: $ATUIN_SEARCH_CMD
                }
            }
        )
    )
}
