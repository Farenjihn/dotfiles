export-env {
    let carapace_completer = {|spans|
        let expanded_alias = (scope aliases | where name == $spans.0 | get -i 0 | get -i expansion)

        let spans = if $expanded_alias != null  {
            $spans | skip 1 | prepend ($expanded_alias | split row " " | take 1)
        } else {
            $spans
        }

        carapace $spans.0 nushell ...$spans | from json
    }

    $env.config = ($env | default {} config).config
    $env.config = ($env.config | default {} completions)

    $env.config = (
        $env.config | upsert completions (
            $env.config.completions | merge {
                external: {
                    enable: true
                    completer: $carapace_completer
                }
            }
        )
    )
}
