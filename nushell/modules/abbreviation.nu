export def --env abbr [
    name: string,
    expanded: string
] {
    $env.abbr = (
        $env.abbr?
        | default {}
        | merge { $name: $expanded }
    )
}

export-env {
    $env.config = ($env | default {} config).config
    $env.config = ($env.config | default {} menus)
    $env.config = (
        $env.config | upsert menus (
            $env.config.menus | append {
                name: abbr_menu
                only_buffer_difference: false
                marker: none
                type: {
                    layout: columnar
                }
                style: {}
                source: { |buffer, position|
                    let list = ($buffer | str trim | split row " ")

                    let head = ($list | drop)
                    let tail = ($list | last)

                    let match = ($env.abbr? | default {} | get -i $tail)

                    if ($match | is-empty) {
                        { value: $buffer }
                    } else {
                        { value: ($head | append $match | str join " ") }
                    }
                }
            }
        )
    )

    $env.config = ($env.config | default [] keybindings)
    $env.config = (
        $env.config | upsert keybindings (
            $env.config.keybindings | append [
                {
                    name: abbr_menu
                    modifier: none
                    keycode: enter
                    mode: [emacs, vi_normal, vi_insert]
                    event: [
                        {
                            send: menu
                            name: abbr_menu
                        },
                        { send: enter }
                    ]
                },
                {
                    name: abbr_menu
                    modifier: none
                    keycode: space
                    mode: [emacs, vi_normal, vi_insert]
                    event: [
                        {
                            send: menu
                            name: abbr_menu
                        },
                        {
                            edit: insertchar
                            value: " "
                        }
                    ]
                }
            ]
        )
    )
}
