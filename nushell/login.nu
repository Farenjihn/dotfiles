$env.LANG = "en_US.UTF-8"
$env.LC_TIME = "en_GB.UTF-8"

$env.XDG_CONFIG_DIRS = "/etc/xdg"
$env.XDG_DATA_DIRS = [
    "/usr/local/share",
    "/usr/share",
    "/var/lib/flatpak/exports/share",
    ($env.HOME | path join ".local/share"),
    ($env.HOME | path join ".local/share/flatpak/exports/share"),
] | str join ":"
