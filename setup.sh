#!/bin/bash

set -e

link_conf() {
    local file="$1"
    local path="$2"

    mkdir -p "${path%/*}"
    if [[ ! -e "${path}" ]]; then
        echo "linking ${file}"
        ln -s "${PWD}/${file}" "${path}"
    fi
}

link_conf "alacritty" "$HOME/.config/alacritty"

link_conf "starship/starship.toml" "$HOME/.config/starship.toml"
link_conf "atuin" "$HOME/.config/atuin"
link_conf "nushell" "$HOME/.config/nushell"

link_conf "nvim" "$HOME/.config/nvim"

link_conf "sway" "$HOME/.config/sway"
link_conf "waybar" "$HOME/.config/waybar"

link_conf "radare2/radare2rc" "$HOME/.radare2rc"
