ZSH_THEME_GIT_PROMPT_ADDED="%{$fg[cyan]%}+"
ZSH_THEME_GIT_PROMPT_MODIFIED="%{$fg[yellow]%}*"
ZSH_THEME_GIT_PROMPT_DELETED="%{$fg[red]%}x"
ZSH_THEME_GIT_PROMPT_RENAMED="%{$fg[blue]%}r"
ZSH_THEME_GIT_PROMPT_UNMERGED="%{$fg[magenta]%}u"
ZSH_THEME_GIT_PROMPT_UNTRACKED="%{$fg[white]%}?"

rkj_git_prompt_info() {
    ref=$(git symbolic-ref HEAD 2> /dev/null) || return
        echo -n " $ZSH_THEME_GIT_PROMPT_PREFIX${ref#refs/heads/}$(git_prompt_status)%{$reset_color%}$ZSH_THEME_GIT_PROMPT_SUFFIX"
}

rkj_vcs_info() {
    rkj_git_prompt_info
}

rkj_retcode() {
    return $?
}

PROMPT_DATE_FORMAT=$'%Y-%m-%d %I:%M:%S'
LINE1=$'%{\e[0;34m%}%B┌─[%b%{\e[0m%}%{\e[0;31m%}%n%{\e[1;30m%}@%{\e[0m%}%{\e[0;33m%}%m%{\e[0;34m%}%B]%b%{\e[0m%} - %b%{\e[0;34m%}%B[%b%{\e[1;37m%}%~%{\e[0;34m%}%B]%b%{\e[0m%}'
PROMPT=$'$LINE1
%{\e[0;34m%}%B└─%B[%{\e[1;30m%}%?$(rkj_retcode)%{\e[0;34m%}%B]%{\e[0m%}%b$(rkj_vcs_info)%{\e[0m%}%b '
PS2=$'\e[0;34m%}%B>%{\e[0m%}%b '
