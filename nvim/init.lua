vim.g.mapleader = ','
vim.g.maplocalleader = ','

require('theme').setup()

local plugins = require('plugins')
plugins.bootstrap()
plugins.setup()

require('keymaps').setup()
require('options').setup()
