local M = {}

M.black     = '#000000'
M.red       = '#a54242'
M.green     = '#8c9440'
M.yellow    = '#de935f'
M.blue      = '#5f819d'
M.magenta   = '#85678f'
M.cyan      = '#5e8d87'
M.gray      = '#707880'
M.bblack    = '#373b41'
M.bred      = '#cc6666'
M.bgreen    = '#b5bd68'
M.byellow   = '#f0c674'
M.bblue     = '#81a2be'
M.bmagenta  = '#b294bb'
M.bcyan     = '#8abeb7'
M.bgray     = '#c5c6c8'
M.white     = '#ffffff'
M.none      = 'NONE'

return M
