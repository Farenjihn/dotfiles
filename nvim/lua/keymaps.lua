local M = {}

function M.setup()
    -- easy split movement
    vim.keymap.set('n', '<C-H>', '<C-W>h')
    vim.keymap.set('n', '<C-J>', '<C-W>j')
    vim.keymap.set('n', '<C-K>', '<C-W>k')
    vim.keymap.set('n', '<C-L>', '<C-W>l')

    vim.keymap.set('n', '<C-Left>', '<C-W>h')
    vim.keymap.set('n', '<C-Down>', '<C-W>j')
    vim.keymap.set('n', '<C-Up>', '<C-W>k')
    vim.keymap.set('n', '<C-Right>', '<C-W>l')

    vim.keymap.set('t', '<ESC>', '<C-\\><C-n>')

    -- telescope and lsp
    local builtin = require('telescope.builtin')

    vim.keymap.set('n', '<C-P>', function() builtin.find_files({}) end)
    vim.keymap.set('n', '<C-F>', function()
        builtin.grep_string { search = '', only_sort_text = true }
    end)

    vim.keymap.set('n', '<Leader>tt', function() builtin.builtin() end)
    vim.keymap.set('n', '<Leader>ws', function() MiniTrailspace.trim() end)

    -- lsp
    vim.keymap.set('n', '<Leader>lR', function() builtin.lsp_references({}) end)
    vim.keymap.set('n', '<Leader>lc', function() builtin.lsp_incoming_calls({}) end)
    vim.keymap.set('n', '<Leader>lC', function() builtin.lsp_outgoing_calls({}) end)
    vim.keymap.set('n', '<Leader>ls', function() builtin.lsp_dynamic_workspace_symbols({}) end)
    vim.keymap.set('n', '<Leader>li', function() builtin.lsp_implementations({}) end)
    vim.keymap.set('n', '<Leader>ld', function() builtin.lsp_definitions({}) end)
    vim.keymap.set('n', '<Leader>lD', function() builtin.lsp_type_definitions({}) end)
    vim.keymap.set('n', '<Leader>le', function() builtin.diagnostics({}) end)
    vim.keymap.set('n', '<Leader>la', function() vim.lsp.buf.code_action() end)
    vim.keymap.set('n', '<Leader>lr', function() vim.lsp.buf.rename() end)
    vim.keymap.set('n', '<Leader>lk', function() vim.lsp.buf.hover() end)
    vim.keymap.set('n', '<Leader>lf', function() vim.lsp.buf.format { async = true } end)

    -- git
    vim.keymap.set('n', '<Leader>gc', function() builtin.git_commits() end)
    vim.keymap.set('n', '<Leader>gC', function() builtin.git_bcommits() end)
    vim.keymap.set('n', '<Leader>gs', function() builtin.git_status() end)
end

return M
