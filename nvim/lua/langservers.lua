local border = {
    { '┌', 'FloatBorder' },
    { '─', 'FloatBorder' },
    { '┐', 'FloatBorder' },
    { '│', 'FloatBorder' },
    { '┘', 'FloatBorder' },
    { '─', 'FloatBorder' },
    { '└', 'FloatBorder' },
    { '│', 'FloatBorder' },
}

local M = {}

function M.setup_lsp()
    local mason = require('mason')
    local mason_lsp = require('mason-lspconfig')

    local capabilities = require('cmp_nvim_lsp').default_capabilities()
    capabilities.textDocument.completion.completionItem.snippetSupport = false

    local function register_lsp(table)
        local name = unpack(table)
        local settings = table.settings or nil

        require('lspconfig')[name].setup {
            capabilities = capabilities,
            settings = settings
        }
    end

    mason.setup {
        PATH = 'append'
    }

    mason_lsp.setup {
        handlers = {
            register_lsp,
        }
    }

    -- servers installed via package manager
    register_lsp { 'rust_analyzer' } -- rust
    register_lsp { 'clangd' }        -- c/cxx
    register_lsp { 'gopls' }         -- go
    register_lsp { 'pylsp' }         -- python
    register_lsp { 'ts_ls' }         -- typescript
    register_lsp {                   -- lua
        'lua_ls',
        settings = {
            Lua = {
                runtime = {
                    version = 'LuaJIT',
                },
                diagnostics = {
                    globals = { 'vim' }
                },
                telemetry = {
                    enable = false,
                },
            }
        }
    }
    register_lsp { 'nushell' }

    vim.lsp.handlers['textDocument/hover'] = vim.lsp.with(
        vim.lsp.handlers.hover, { border = border }
    )

    vim.lsp.handlers['textDocument/signatureHelp'] = vim.lsp.with(
        vim.lsp.handlers.signature_help, { border = border }
    )

    vim.lsp.handlers['textDocument/publishDiagnostics'] = vim.lsp.with(
        vim.lsp.diagnostic.on_publish_diagnostics, {
            virtual_text = true,
            signs = false,
        }
    )
end

function M.setup_cmp()
    local cmp = require('cmp')

    cmp.setup {
        window = {
            completion = cmp.config.window.bordered({ border = border }),
            documentation = cmp.config.window.bordered({ border = border }),
        },
        mapping = {
            ['<CR>'] = cmp.mapping.confirm({ select = true }),
            ['<Tab>'] = cmp.mapping.confirm({ select = true }),
            ['<Up>'] = cmp.mapping(cmp.mapping.select_prev_item(), { 'i', 'c' }),
            ['<Down>'] = cmp.mapping(cmp.mapping.select_next_item(), { 'i', 'c' }),
        },
        sources = cmp.config.sources({
            { name = 'nvim_lsp' }
        }, {
            { name = 'buffer' },
            { name = 'path' }
        })
    }

    cmp.setup.cmdline({ '/', '?' }, {
        sources = {
            { name = 'buffer' }
        }
    })

    cmp.setup.cmdline(':', {
        sources = {
            { name = 'path' }
        }
    })
end

return M
