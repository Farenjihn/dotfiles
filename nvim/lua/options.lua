local M = {}

function M.setup()
    vim.opt.tabstop = 4
    vim.opt.shiftwidth = 4
    vim.opt.expandtab = true
    vim.opt.showmode = false
    vim.opt.hidden = true
    vim.opt.number = true
    vim.opt.ic = true
    vim.opt.inccommand = 'nosplit'
    vim.opt.modeline = false
end

return M
