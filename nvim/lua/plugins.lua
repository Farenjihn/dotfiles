local M = {}

function M.bootstrap()
    -- boostrap lazy.nvim
    local lazypath = vim.fn.stdpath('data') .. '/lazy/lazy.nvim'

    if not vim.loop.fs_stat(lazypath) then
        vim.fn.system({
            'git',
            'clone',
            '--filter=blob:none',
            'https://github.com/folke/lazy.nvim.git',
            '--branch=stable',
            lazypath,
        })
    end

    vim.opt.rtp:prepend(lazypath)
end

function M.setup()
    local config = require('config')
    local lsp = require('langservers')

    require('lazy').setup({
        -- statusline
        { 'nvim-lualine/lualine.nvim', config = config.lualine },

        -- lsp and completion
        {
            'hrsh7th/nvim-cmp',
            dependencies = {
                'hrsh7th/cmp-buffer',
                'hrsh7th/cmp-path',
                'hrsh7th/cmp-nvim-lsp',
            },
            config = lsp.setup_cmp
        },
        {
            'neovim/nvim-lspconfig',
            dependencies = {
                'williamboman/mason.nvim',
                'williamboman/mason-lspconfig.nvim',
            },
            config = lsp.setup_lsp,
        },

        -- telescope
        {
            'nvim-telescope/telescope.nvim',
            dependencies = {
                'nvim-lua/plenary.nvim',
                'nvim-telescope/telescope-ui-select.nvim',
                { 'nvim-telescope/telescope-fzf-native.nvim', build = 'make' },
            },
            config = config.telescope
        },

        -- treesitter
        {
            'nvim-treesitter/nvim-treesitter',
            build = ':TSUpdate',
            config = config.treesitter
        },

        -- utils
        {
            'echasnovski/mini.nvim',
            branch = 'stable',
            config = config.mini
        },

        -- to investigate:
        --  - formatting without langserver
        --  - LaTeX env
    }, {
        change_detection = {
            enabled = false,
        }
    })
end

return M
