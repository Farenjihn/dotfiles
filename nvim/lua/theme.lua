local colors = require('colors')

local syntax = {
    Boolean        = { fg = colors.red },
    Character      = { fg = colors.red },
    Comment        = { fg = colors.blue },
    Conditional    = { fg = colors.magenta },
    Constant       = { fg = colors.white },
    Define         = { fg = colors.white },
    Delimiter      = { fg = colors.white },
    Float          = { fg = colors.cyan },
    Function       = { fg = colors.bgreen, bold = true },
    Identifier     = { fg = colors.white },
    Include        = { fg = colors.magenta },
    Keyword        = { fg = colors.magenta },
    Label          = { fg = colors.white },
    Macro          = { fg = colors.bmagenta },
    Number         = { fg = colors.cyan },
    Operator       = { fg = colors.blue },
    PreCondit      = { fg = colors.white },
    PreProc        = { fg = colors.white },
    Repeat         = { fg = colors.magenta },
    Special        = { fg = colors.white },
    SpecialChar    = { fg = colors.white },
    SpecialComment = { fg = colors.white },
    Statement      = { fg = colors.white },
    StorageClass   = { fg = colors.yellow },
    String         = { fg = colors.red },
    Structure      = { fg = colors.bgreen },
    Tag            = { fg = colors.white },
    Todo           = { fg = colors.white },
    Type           = { fg = colors.bgreen },
    Typedef        = { fg = colors.bgreen },
}

local diffs = {
    DiffAdd        = { fg = colors.bgreen },
    DiffAdded      = { fg = colors.bgreen },
    DiffChange     = { fg = colors.yellow },
    DiffDelete     = { fg = colors.red },
    DiffRemoved    = { fg = colors.red },
    DiffFile       = { fg = colors.white },
    DiffLine       = { fg = colors.white },
    DiffNewFile    = { fg = colors.white },
    DiffText       = { fg = colors.white },
}

local editor = {
    Debug        = { fg = colors.white, bg = colors.red },
    Error        = { fg = colors.white, bg = colors.red },
    ErrorMsg     = { fg = colors.white, bg = colors.red },
    Ignore       = { fg = colors.gray },
    LineNr       = { fg = colors.gray },
    MatchParen   = { fg = colors.white, bg = colors.bblue },
    NonText      = { fg = colors.gray },
    Normal       = { fg = colors.white },
    NormalFloat  = { fg = colors.white },
    NormalMode   = { fg = colors.white },
    Pmenu        = { fg = colors.white },
    PmenuSbar    = { fg = colors.white },
    PmenuSel     = { fg = colors.white, bg = colors.gray },
    Search       = { underline = true },
    SignColumn   = { fg = colors.white },
    StatusLine   = { bg = colors.bblack },
    StatusLineNC = { bg = colors.black },
    Title        = { fg = colors.white },
    Underlined   = { fg = colors.white },
    VertSplit    = { fg = colors.white },
    Visual       = { bg = colors.none, reverse = true },
}

local treesitter = {
    ['@namespace']           = { fg = colors.bgreen },
    ['@type.qualifier']      = { fg = colors.yellow },
}

local plugins = {
    MiniTrailspace         = { bg = colors.bred },
    StatusLineGitBranch    = { fg = colors.yellow },
    StatusLineFileTypeName = { fg = colors.yellow },
}

local lsp = {
    DiagnosticDefaultError     = { fg = colors.white },
    DiagnosticDefaultHint      = { fg = colors.white },
    DiagnosticDefaultInfo      = { fg = colors.white },
    DiagnosticDefaultWarn      = { fg = colors.white },
    DiagnosticFloatingError    = { fg = colors.white },
    DiagnosticFloatingHint     = { fg = colors.white },
    DiagnosticFloatingInfo     = { fg = colors.white },
    DiagnosticFloatingWarn     = { fg = colors.white },
    DiagnosticSignError        = { fg = colors.white },
    DiagnosticSignHint         = { fg = colors.white },
    DiagnosticSignInfo         = { fg = colors.white },
    DiagnosticSignWarn         = { fg = colors.white },
    DiagnosticUnderlineError   = { fg = colors.white, underline = true },
    DiagnosticUnderlineHint    = { fg = colors.white, underline = true },
    DiagnosticUnderlineInfo    = { fg = colors.white, underline = true },
    DiagnosticUnderlineWarn    = { fg = colors.white, underline = true },
    DiagnosticVirtualTextError = { fg = colors.bred },
    DiagnosticVirtualTextHint  = { fg = colors.bblue },
    DiagnosticVirtualTextInfo  = { fg = colors.white },
    DiagnosticVirtualTextWarn  = { fg = colors.byellow },
    LspReferenceRead           = { fg = colors.white },
    LspReferenceText           = { fg = colors.white },
    LspReferenceWrite          = { fg = colors.white },
}

local function highlight(statement)
    for name, setting in pairs(statement) do
        vim.api.nvim_set_hl(0, name, setting)
    end
end

local M = {}

function M.setup()
    vim.cmd('highlight clear')

    vim.opt.termguicolors = true

    if vim.fn.exists('syntax_on') then
        vim.cmd('syntax reset')
    end

    highlight(syntax)
    highlight(editor)
    highlight(diffs)
    highlight(treesitter)
    highlight(plugins)
    highlight(lsp)
end

return M
