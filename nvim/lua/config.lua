local M = {}

function M.lualine()
    local colors = require('colors')

    require('lualine').setup {
        options = {
            icons_enabled = false,
            component_separators = { left = '|', right = '|' },
            section_separators = { left = '', right = ' ' },
            theme = {
                normal = {
                    a = { bg = colors.dark_blue, fg = colors.bright_white },
                    b = { bg = colors.bright_black, fg = colors.bright_white },
                    c = { bg = colors.dark_black, fg = colors.dark_white }
                },
                insert = {
                    a = { bg = colors.dark_green, fg = colors.bright_white },
                    b = { bg = colors.bright_black, fg = colors.bright_white },
                    c = { bg = colors.dark_black, fg = colors.dark_white }
                },
                visual = {
                    a = { bg = colors.dark_yellow, fg = colors.bright_white },
                    b = { bg = colors.bright_black, fg = colors.bright_white },
                    c = { bg = colors.dark_black, fg = colors.dark_white }
                },
                replace = {
                    a = { bg = colors.dark_red, fg = colors.bright_white },
                    b = { bg = colors.bright_black, fg = colors.bright_white },
                    c = { bg = colors.dark_black, fg = colors.dark_white }
                },
                command = {
                    a = { bg = colors.dark_magenta, fg = colors.bright_white },
                    b = { bg = colors.bright_black, fg = colors.bright_white },
                    c = { bg = colors.dark_black, fg = colors.dark_white }
                },
                inactive = {
                    a = { bg = colors.dark_white, fg = colors.bright_white },
                    b = { bg = colors.bright_black, fg = colors.bright_white },
                    c = { bg = colors.dark_black, fg = colors.dark_white }
                },
            }
        },
        sections = {
            lualine_a = { 'mode' },
            lualine_b = { 'filename', 'branch', 'diff' },
            lualine_c = {},
            lualine_x = { 'filetype', 'encoding', 'fileformat' },
            lualine_y = {},
            lualine_z = { 'location' }
        },
        inactive_sections = {
            lualine_a = { 'mode' },
            lualine_b = { 'filename' },
            lualine_c = {},
            lualine_x = {},
            lualine_y = {},
            lualine_z = {}
        }
    }
end

function M.telescope()
    local telescope = require('telescope')

    telescope.setup {
        defaults = {
            borderchars = { '─', '│', '─', '│', '┌', '┐', '┘', '└' },
            layout_strategy = 'vertical',
        },
        pickers = {
            find_files = { theme = 'ivy', follow = true },
        },
        extensions = {
            fzf = {
                fuzzy = true,
                override_generic_sorter = true,
                override_file_sorter = true,
                case_mode = 'smart_case',
            },
        }
    }

    telescope.load_extension('fzf')
    telescope.load_extension('ui-select')
end

function M.treesitter()
    require('nvim-treesitter.configs').setup {
        ensure_installed = {
            'bash',
            'c',
            'cmake',
            'cpp',
            'css',
            'dockerfile',
            'go',
            'html',
            'http',
            'java',
            'javascript',
            'json',
            'json5',
            'lua',
            'make',
            'meson',
            'python',
            'rust',
            'toml',
            'typescript',
            'udev',
            'xml',
            'yaml'
        },
        sync_install = false,
        highlight = {
            enable = true,
            additional_vim_regex_highlighting = false,
        },
    }
end

function M.mini()
    require('mini.comment').setup()
    require('mini.surround').setup()
    require('mini.trailspace').setup()
end

function M.zk()
    require('zk').setup { picker = 'telescope' }
end

return M
