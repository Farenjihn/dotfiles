ZSH=/usr/share/oh-my-zsh/

DISABLE_AUTO_UPDATE="true"
ZSH_THEME="farenjihn"
ZSH_CUSTOM=/home/farenjihn/.oh-my-zsh

plugins=(web-search)

ZSH_CACHE_DIR="$HOME/.oh-my-zsh-cache"
if [[ ! -d "$ZSH_CACHE_DIR" ]]; then
    mkdir "$ZSH_CACHE_DIR"
fi

source "$ZSH/oh-my-zsh.sh"

HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000

zstyle :compinstall filename "/home/farenjihn/.zshrc"

# ENV
export PATH="$HOME/.local/bin:$HOME/.krew/bin:$PATH:/sbin:/usr/sbin:/opt/google-cloud-cli/bin/"
export EDITOR="nvim"
export BROWSER="firefox"
export SSH_AUTH_SOCK="$XDG_RUNTIME_DIR/ssh-agent.socket"

## FZF
export FZF_DEFAULT_COMMAND="rg --files"
export FZF_COMPLETION_OPTS="-i"
source /usr/share/fzf/completion.zsh
source /usr/share/fzf/key-bindings.zsh

_fzf_compgen_path() {
    rg --files "$1"
}

_fzf_compgen_dir() {
    rg --files --null --sort path "$1" 2>/dev/null | xargs -0 dirname | sort -u
}

# Wayland
export XDG_CURRENT_DESKTOP=sway

export MOZ_ENABLE_WAYLAND=1
export QT_QPA_PLATFORM=wayland
export CLUTTER_BACKEND=wayland
export SDL_VIDEODRIVER=wayland

export VDPAU_DRIVER=radeonsi

# Podman
export REGISTRY_AUTH_FILE="${HOME}/.config/containers/auth.json"

# Aliases
alias vim="nvim"

# S3
export AWS_EC2_METADATA_DISABLED=true
export AWS_PAGER=""

# Functions
goshit() {
    export GOPATH="$PWD/.gopath"
}

mesaclear() {
    rm -rf ~/.cache/mesa_shader_cache
    rm -rf ~/.var/app/com.valvesoftware.Steam/cache/mesa_shader_cache
    rm -rf ~/games/steam/steamapps/shadercache
}

# r2meson() {
#     local cflags="${CFLAGS}"
#     cflags+=" -fstack-clash-protection"
#     cflags+=" -fstack-protector-all"
#     cflags+=" -fstack-protector-strong"
#     cflags+=" -fcf-protection"
#     cflags+=" -D_FORTIFY_SOURCE=3"
#
#     local ldflags="${LDFLAGS}"
#     ldflags+=" -Wl,-z,relro,-z,now"
#
#     CFLAGS="${cflags}" LDFLAGS="${ldflags}" meson \
#         setup build \
#         --buildtype release \
#         --default-library shared \
#         --prefix ~/.local \
#         --strip \
#         --wipe \
#         -D b_pie=true \
#         -D local=true \
#         -D use_sys_capstone=true \
#         -D use_capstone_version=v5 \
#         -D use_sys_magic=true \
#         -D use_sys_zip=true \
#         -D use_sys_zlib=true \
#         -D use_sys_lz4=true \
#         -D use_sys_xxhash=true \
#         -D use_sys_openssl=true \
#         -D use_libuv=true \
#         -D use_webui=false
# }

kube-encrypt() {
    kubeseal \
        --controller-name sealed-secrets \
        --controller-namespace kube-system \
        -o yaml < $1 > tmp.yaml && mv tmp.yaml $1
}

kube-reencrypt() {
    kubeseal \
        --controller-name sealed-secrets \
        --controller-namespace kube-system \
        --re-encrypt -o yaml < $1 > tmp.yaml && mv tmp.yaml $1
}
